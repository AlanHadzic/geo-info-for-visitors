
var request = require('request');
var apiParametri = {
    streznik: 'http://localhost:3000'
};
//if (process.env.NODE_ENV === 'production') {
//    apiParametri.streznik = 'https://edugeocache-sp-2018-2019.herokuapp.com/';
//}

module.exports.list = function(req, res) {
    res.render('locations-list', {
        title: 'EduGeoCache - Poiščite zanimive lokacije blizu vas!',
        glavaStrani: {
            naslov: 'EduGeoCache',
            podnaslov: 'Poiščite zanimive lokacije blizu vas!'
        },
        stranskaOrodnaVrstica: 'Iščete lokacijo za kratkočasenje? EduGeoCache vam pomaga pri iskanju zanimivih lokacij v bližini. Mogoče imate kakšne posebne želje? Naj vam EduGeoCache pomaga pri iskanju bližnjih zanimivih lokacij.',
        lokacije: [{
            naziv: 'Stari grad Celje',
            naslov: 'Cesta na grad 78, 3000 Celje, Slovenija',
            ocena: 3,
            lastnosti: ['slikovit razgled', 'vstopnina', 'otrokom prijazno'],
            razdalja: '700m'
        }, {
            naziv: 'ZOO Ljubljana',
            naslov: 'Večna pot 70, 1000 Ljubljana, Slovenija',
            ocena: 4,
            lastnosti: ['priporočljivo za otroke', 'živali', 'parkirišče je na voljo', 'vstopnina'],
            razdalja: '85km'
        }]
    });
};

module.exports.seznam = function(req, res) {
    var pot = '/api/lokacije';
    var parametriZahteve = {
        url: apiParametri.streznik + pot,
        method: 'GET',
        json: {},
        qs: {
            lng: 14.469027,
            lat: 46.050129,
            maxRazdalja: 100
        }
    };
    request(
        parametriZahteve,
        function(napaka, odgovor, vsebina) {
            for (var stevec=0; stevec < vsebina.length; stevec++) {
                vsebina[stevec].razdalja =
                    formatirajRazdaljo(vsebina[stevec].razdalja);
            }
            list(req, res, vsebina);
        }
    );
};

var formatirajRazdaljo = function(razdalja) {
    var vrednostRazdalje, enota;
    if (razdalja > 1) {
        vrednostRazdalje = parseFloat(razdalja).toFixed(1);
        enota = 'km';
    } else {
        vrednostRazdalje = parseInt(razdalja * 1000, 10);
        enota = 'm';
    }
    return vrednostRazdalje + enota;
};

module.exports.locationDetail = function(req, res) {
    res.render('location-detail', {
        title: 'Stari grad Celje Podrobnosti lokacije',
        glavaStrani: {
            naslov: 'Stari grad Celje'
        },
        stranskaOrodnaVrstica: {
            kontekst: 'je na EduGeoCache, ker je zanimiva lokacija, ki si jo lahko ogledate, ko ste brez idej za kratek izlet.',
            poziv: 'Če vam je lokacija všeč, ali pa tudi ne, dodajte svoj komentar in s tem pomagajte ostalim uporabnikom pri odločitvi.'
        },
        lokacija: {
            naziv: 'Stari grad Celje',
            naslov: 'Cesta na grad 78, 3000 Celje, Slovenija',
            ocena: 3,
            lastnosti: ['slikovit razgled', 'vstopnina', 'otrokom prijazno'],
            koordinate: {lat: 46.219849, lng: 15.271601},
            delovniCas: [{
                dnevi: 'ponedeljek - petek',
                odprtje: '9:00',
                zaprtje: '21:00',
                zaprto: false
            }, {
                dnevi: 'sobota',
                odprtje: '9:00',
                zaprtje: '19:00',
                zaprto: false
            }, {
                dnevi: 'nedelja',
                zaprto: true
            }],
            komentarji: [{
                avtor: 'Kim Jong Un',
                ocena: 2,
                datum: '8. november, 2018',
                besediloKomentarja: 'Čisti dolgčas, še kava je zanič.'
            },{
                avtor: 'Jong Neil',
                ocena: 4,
                datum: '10. november, 2018',
                besediloKomentarja: 'A ono.'
            }]
        }
    });
};


module.exports.comment = function(req, res) {
    res.render('location-comment-new', {
        title: 'Dodaj komentar za Stari grad Celje na EduGeoCache',
        glavaStrani: {
            naslov: 'Komentiraj Stari grad Celje'
        }
    });
};
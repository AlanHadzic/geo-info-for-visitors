var express = require('express');
var router = express.Router();
var controllerMain = require('../controllers/main');
var controllerLocation = require('../controllers/locations');
var controllerOther = require('../controllers/other');

//router.get('/', controllerMain.index);

router.get('/', controllerLocation.list);
router.get('/locations', controllerLocation.locationDetail);
router.get('/locations/comment/new', controllerLocation.comment);

router.get('/information', controllerOther.info);

module.exports = router;

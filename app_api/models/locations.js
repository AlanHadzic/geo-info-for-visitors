var mongoose = require('mongoose');

var komentarjiShema = new mongoose.Schema({
    avtor: String,
    ocena: {type: Number, "default": 0, min: 0, max: 5},
    besediloKomentarja: String,
    datum: {type: Date, "default": Date.now()}
});

var delovniCasShema = new mongoose.Schema({
    dnevi: {type: String, required: true},
    odprtje: String,
    zaprtje: String,
    zaprto: {type: Boolean, required: true},
});

var locationsShema = new mongoose.Schema({
    naziv: {type: String, required: true},
    naslov: String,
    ocena: {type: Number, "default": 0, min:0, max: 5},
    lastnosti: [String],
    koordinate: {type: [Number], index: "2dsphere"},
    delovniCas: [delovniCasShema],
    komentarji: [komentarjiShema]
});

mongoose.model('Lokacija', locationsShema, 'Lokacije');